import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

const App = () => {
  return (
    <Router>
      <Switch>
        <Route exact component={Login} path="/" />
      </Switch>
    </Router>
  );
};

export default App;
